Nonterminals
predicates predicate list element elements members
imports import list_elements list_element .

Terminals '{' '}' ',' '[' ']' ':' any_text label.

Rootsymbol predicates.


predicates -> predicate : ['$1']. 
predicates -> predicate predicates : ['$1'] ++ '$2'. 
predicate -> label members : {'$1', '$2'}. 
predicate -> label imports : {imports, '$2'}. 

list -> '[' ']' : nil.
list -> '[' list_elements ']' : {list,'$2'}.

members -> '{' '}' : {members, nil}.
members -> '{' elements '}' : {members, '$2'}.

list_elements -> list_element : ['$1'].
list_elements -> list_element ',' list_elements : ['$1'] ++ '$3'.
list_element -> any_text : {element, unwrap('$1')}.

elements -> element : ['$1'].
elements -> element ',' elements : ['$1'] ++ '$3'.
element -> label list : {'$1', '$2'}.
element -> label members : {'$1', '$2'}.
element -> label any_text ',' any_text : {'$1', '$2'}.
element -> label any_text : {'$1', '$2'}.
element -> label elements : {'$1', '$2'}.

imports -> import : '$1'.
imports -> import ',' imports : ['$1'] ++ '$3'.
import -> any_text : {elem, unwrap('$1')}.


Erlang code.

unwrap({_,_,V}) -> V.
