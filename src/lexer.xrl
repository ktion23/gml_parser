%% leex file structure

Definitions.
BlockComment = \/\*(.|\r\n|\n)*?\*\/
WS = [\s\t]
LB = \n|\r\n|\r
Letter = [A-Za-z0-9]
Label = [A-Za-z0-9][A-Za-z0-9\_\.\:]*\:
Atom = [A-Za-z0-9][A-Za-z0-9\_]*\.[A-Za-z0-9\_\.]*
Number = [0-9]+
LineComment = \/\/[\s]*.*

Rules.
".*?" : {token, {any_text, TokenLine, list_to_binary(lists:droplast(lists:nthtail(1,TokenChars)))}}.
{Number} : {token, {number, TokenLine, list_to_integer(TokenChars)}}.
{Label} : {token, {label, TokenLine, list_to_binary(lists:droplast(TokenChars))}}.
{Atom} : {token, {atom, TokenLine, TokenChars}}.
{BlockComment} : skip_token.
%%\. : {token, {dot, TokenLine}}.
\= : {token, {assign, TokenLine}}.
[{},\[\]]  : {token,{list_to_atom(TokenChars),TokenLine}}.

{WS} : skip_token.
{LineComment} : skip_token. 
{LB} : skip_token.

%% . : {token, {any_text, TokenLine, TokenChars}}.

{Letter}+ : {token, {any_text, TokenLine, list_to_binary(TokenChars)}}.

Erlang code.
