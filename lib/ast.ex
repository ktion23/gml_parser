defmodule VSON.AST do
  def parse_string(text) do
    {:ok, tree, _} = :lexer.string(:binary.bin_to_list(text))

    # IO.inspect tree
    try do
      {res, []} = doc(tree, [], %{})
      {:ok, res}
    catch
      :throw, x ->
        x
    end
  end

  defp doc(a, b) do
    doc(a, b, %{nested_doc_level: 0})
  end

  defp doc([], acc, _) do
    {:lists.reverse(acc), []}
  end

  defp doc(all, acc, opts) do
    if Process.get(:doc_debug) do
      IO.inspect({:doc, all})
    end

    started_with_cbracket = Map.get(opts, :started_with_cbracket, nil)

    within_brackets = Map.get(opts, :within_brackets, nil)

    case all do
      [{:atom, _, name}, {:assign, _} | rest] ->
        {elements, rest} = list_of_values_simple(rest, [], %{within_brackets: within_brackets})
        doc(rest, [{:assign, name, elements} | acc], opts)

      [{:label, _, 'using:'} | rest] ->
        {elements, rest} =
          list_of_values_simple(rest, [], %{break_on_label: true, within_brackets: false})

        doc(rest, [{:using, elements} | acc], opts)

      [{:label, _, label} | rest] ->
        {elements, rest} = list_of_values_simple(rest, [], %{within_brackets: within_brackets})
        doc(rest, [{:kv, label, elements} | acc], opts)

      [{:",", _line} | rest] ->
        doc(rest, acc, opts)

      [{:"}", _} | rest] when started_with_cbracket != true ->
        # IO.puts "returning from block started_with_cbracket != true"
        {:lists.reverse(acc), all}

      [{:"}", _} | rest] ->
        # IO.puts "returning from block started_with_cbracket = true"
        {:lists.reverse(acc), rest}

      [{:"]", _} | rest] ->
        {:lists.reverse(acc), rest}

      [entry | _rest] ->
        throw({:error, "unexpected element", entry})
    end
  end

  defp list_of_values_simple(all, acc, opts) do
    res = list_of_values(all, acc, opts)

    case res do
      {[a], rest} -> {a, rest}
      _ -> res
    end
  end

  defp list_of_values([next | rest] = all, acc, sm = %{break_if_no_comma: true}) do
    case next do
      {:",", _} ->
        list_of_values(all, acc, %{sm | break_if_no_comma: false})

      _ ->
        {:lists.reverse(acc), all}
    end
  end

  defp list_of_values([{:",", _}, {:label, _, label} | rest] = all, acc, _sm) do
    {:lists.reverse(acc), all}
  end

  defp list_of_values([{:label, _, label} | rest] = all, acc, %{break_on_label: true}) do
    {:lists.reverse(acc), all}
  end

  defp list_of_values([{:label, _, label} | rest] = all, acc, sm = %{within_brackets: true}) do
    {elements, rest} = doc(all, [], sm)
    list_of_values(rest, [elements | acc], sm)
  end

  defp list_of_values([{:label, _, label} | rest] = all, acc, sm) do
    {:lists.reverse(acc), all}
  end

  defp list_of_values([{:number, _line, text_a} | rest], acc, sm) do
    list_of_values(rest, [text_a | acc], sm)
  end

  defp list_of_values([{:any_text, _line, text_a} | rest], acc, sm) do
    list_of_values(rest, [text_a | acc], sm)
  end

  defp list_of_values([{:",", _}, {:any_text, _line, text_b} | rest], acc, sm) do
    list_of_values(rest, [text_b | acc], sm)
  end

  defp list_of_values([{:",", _}, {:number, _line, text_b} | rest], acc, sm) do
    list_of_values(rest, [text_b | acc], sm)
  end

  defp list_of_values([{:"{", _} | rest], acc, sm) do
    {elements, rest} = doc(rest, [], %{started_with_cbracket: true, within_brackets: true})
    sm = Map.put(sm, :break_if_no_comma, true)
    list_of_values(rest, [elements | acc], sm)
  end

  defp list_of_values([{:"[", _} | rest], acc, sm) do
    {elements, rest} = list_of_values(rest, [], %{})
    list_of_values(rest, [elements | acc], sm)
  end

  defp list_of_values([{:"}", _} | rest] = all, acc, _sm) do
    {:lists.reverse(acc), all}
  end

  defp list_of_values([{:"]", _} | rest] = all, acc, _sm) do
    {:lists.reverse(acc), rest}
  end

  defp list_of_values(rest, acc, _sm) do
    # IO.puts "out of list_of_strings #{inspect rest}"
    {:lists.reverse(acc), rest}
  end
end
