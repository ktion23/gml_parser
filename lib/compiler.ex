defmodule VSON.Parser do
  def parse_string(text, _opts \\ %{}) do
    res = VSON.AST.parse_string(text)

    if Process.get(:debug_vson) do
      IO.inspect(res)
    end

    {:ok, ast} = res
    compact(ast)
  end

  def compact(ast) when is_list(ast) do
    ast =
      Enum.map(ast, fn entry ->
        case entry do
          {:kv, key, value} ->
            {key, compact(value)}

          b when is_list(b) ->
            compact(Enum.map(b, &compact(&1)))

          a ->
            a
        end
      end)

    non_kv =
      Enum.find(ast, fn x ->
        case x do
          {_, _} -> false
          _ -> true
        end
      end)

    if non_kv do
      ast
    else
      Enum.reduce(ast, Map.new(), fn {key, v}, acc ->
        keys = String.split(key, ~r"\.|:")
        acc = ensure_path(acc, keys, [])
        put_in(acc, keys, v)
      end)
    end
  end

  def ensure_path(struct, [key], _) do
    struct
  end

  def ensure_path(struct, [key | rest], keysout) do
    struct =
      if get_in(struct, keysout ++ [key]) == nil do
        struct = put_in(struct, keysout ++ [key], %{})
      else
        struct
      end

    ensure_path(struct, rest, keysout ++ [key])
  end

  def compact(ast) do
    ast
  end
end
