defmodule ASTTest do
  use ExUnit.Case

  test "decode using" do
    {:ok, res} = VSON.AST.parse_string("using: items, character")
    # IO.inspect(s, limit: 99999)
    IO.inspect(res, limit: 99999)
    :ok
  end

  test "simple struct" do
    {:ok, res} = VSON.AST.parse_string("player: { initial: {}}")
    # IO.inspect(s, limit: 99999)
    IO.inspect(res, limit: 99999)
  end

  test "nested struct" do
    {:ok, res} = VSON.AST.parse_string("player: { 
initial: { 
eyes: randm, complexion: random
}
}")
    # IO.inspect(s, limit: 99999)
    IO.inspect(res, limit: 99999)
  end

  test "implicit list of literals" do
    {:ok, res} = VSON.AST.parse_string("player: { clothing: generic, random}")
    # IO.inspect(s, limit: 99999)
    IO.inspect(res, limit: 99999)
  end

  test "namespaces" do
    {:ok, res} = VSON.AST.parse_string("definition:settings: {
    gender: female, male
}")
    # IO.inspect(s, limit: 99999)
    IO.inspect(res, limit: 99999)
  end

  test "namespaces 2" do
    {:ok, res} = VSON.AST.parse_string("definition:settings: {
    gender:lol: {},
    broad: {}
}")
    # IO.inspect(s, limit: 99999)
    IO.inspect(res, limit: 99999)
  end

  test "simple 1" do
    {:ok, res} = VSON.AST.parse_string("
    initial:identity: {
            color: {eyes: random, complexion: random}
   }
")
    # IO.inspect(s, limit: 99999)
    IO.inspect(res, limit: 99999)
  end

  test "demo struct 1" do
    {:ok, res} = VSON.AST.parse_string("
player: {

    initial:identity: {
        selection:
            color: {eyes: random, complexion: random},
            facial: none,
            clothing: [generic, random],
            hairstyle: default, // Uses the default selection.
            gender: random
    },

    inventory: {
        item: {},
        material.bag: {}
    }

}")
    # IO.inspect(s, limit: 99999)
    IO.inspect(res, limit: 99999)
  end

  test "demo struct 2" do
    doc = """
    items: {
    }

    items.weapon = {config:script: "./weapons.definitions"}
    """

    {:ok, res} = VSON.AST.parse_string(doc)
    # IO.inspect(s, limit: 99999)
    IO.inspect(res, limit: 99999)
  end

  test "demo file 1" do
    {:ok, res} = VSON.AST.parse_string(File.read!("examples/player.definition"))
    IO.inspect(res, limit: 99999)
  end

  test "demo file 2" do
    {:ok, res} = VSON.AST.parse_string(File.read!("examples/world/world.definition"))
    # IO.inspect(s, limit: 99999)
    IO.inspect(res, limit: 99999)
  end
end
